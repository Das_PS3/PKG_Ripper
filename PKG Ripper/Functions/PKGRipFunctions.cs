﻿using Extra.Utilities;
using PKG_Extractor;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace PKG_Ripper.Functions
{
    internal class PKGRipFunctions
    {
        internal static bool RipPKGFile(string pkgFile, string targetDirectory, string targetFile, long requestedSize, PKGHeader header, PKGEntry entry)
        {
            if (entry.Filename.EndsWith(targetFile, StringComparison.InvariantCultureIgnoreCase))
            {
                byte[] file = File.Exists(pkgFile) ? GetFilePKGFile(pkgFile, requestedSize, entry, header) : GetLinkPKGFile(pkgFile, requestedSize, entry, header);

                if (entry.Filename.EndsWith(".self", StringComparison.InvariantCultureIgnoreCase) ||
                    entry.Filename.EndsWith(".sprx", StringComparison.InvariantCultureIgnoreCase))
                {
                    byte[] isNPD = new byte[1];

                    Buffer.BlockCopy(file, 0x7F, isNPD, 0, isNPD.Length);

                    if (isNPD[0] != 0x08)
                        return false;
                }

                string output = Path.Combine(targetDirectory, header.ContentID + (File.Exists(pkgFile) ? string.Empty : "_" + Path.GetFileNameWithoutExtension(new Uri(pkgFile).AbsolutePath).Substring(0, 4)));

                string fileOutput = Path.Combine(output, header.TitleID, entry.Filename);

                Directory.CreateDirectory(Path.GetDirectoryName(fileOutput));

                File.WriteAllBytes(fileOutput, file);

                return true;
            }

            return false;
        }

        internal static void RipPKGFiles(string pkgFile, string targetDirectory, string targetFile, long requestedSize, PKGHeader header)
        {
            foreach (var entry in header.Entries)
            {
                if (entry.Filename.EndsWith(targetFile, StringComparison.InvariantCultureIgnoreCase))
                {
                    byte[] file = File.Exists(pkgFile) ? GetFilePKGFile(pkgFile, requestedSize, entry, header) : GetLinkPKGFile(pkgFile, requestedSize, entry, header);

                    string output = Path.Combine(targetDirectory, header.ContentID + (File.Exists(pkgFile) ? string.Empty : "_" + Path.GetFileNameWithoutExtension(new Uri(pkgFile).AbsolutePath).Substring(0, 4)), header.TitleID);

                    string fileOutput = Path.Combine(output, entry.Filename);

                    Directory.CreateDirectory(Path.GetDirectoryName(fileOutput));

                    File.WriteAllBytes(fileOutput, file);
                }
            }
        }

        internal static PKGHeader RipPKGTOC(string pkgFile, string targetDirectory)
        {
            string pkgFilename = File.Exists(pkgFile) ? Path.GetFileName(pkgFile) : Path.GetFileName(new Uri(pkgFile).AbsolutePath);

            string output = Path.Combine(targetDirectory, pkgFilename);

            string outputTOC = output + "_toc.txt";

            string outputTOCSummary = output + "_toc_summary.txt";

            if (File.Exists(outputTOC))
                File.Delete(outputTOC);

            PKGHeader header;

            if (File.Exists(pkgFile))
                header = GetFilePKGHeader(pkgFile);
            else
                header = GetLinkPKGHeader(pkgFile);

            uint paramSFOCount = 0;
            long paramSFOSize = 0;
            uint icon0Count = 0;
            long icon0Size = 0;
            uint ebootCount = 0;
            long ebootSize = 0;
            uint selfCount = 0;
            long selfSize = 0;
            uint sprxCount = 0;
            long sprxSize = 0;
            uint sdatCount = 0;
            long sdatSize = 0;
            uint edatCount = 0;
            long edatSize = 0;
            uint trpCount = 0;
            long trpSize = 0;
            uint elfCount = 0;
            long elfSize = 0;
            uint prxCount = 0;
            long prxSize = 0;

            foreach (var entry in header.Entries)
            {
                string line = entry.Filename + ";" + entry.FileContentsOffset + ";" + entry.FileContentsSize + Environment.NewLine;

                File.AppendAllText(outputTOC, line);

                if (entry.Filename.EndsWith(".edat", StringComparison.InvariantCultureIgnoreCase))
                {
                    edatCount++;
                    edatSize += entry.FileContentsSize;
                }
                else if (entry.Filename.EndsWith(".self", StringComparison.InvariantCultureIgnoreCase))
                {
                    selfCount++;
                    selfSize += entry.FileContentsSize;
                }
                else if (entry.Filename.EndsWith(".sprx", StringComparison.InvariantCultureIgnoreCase))
                {
                    sprxCount++;
                    sprxSize += entry.FileContentsSize;
                }
                else if (entry.Filename.EndsWith(".sdat", StringComparison.InvariantCultureIgnoreCase))
                {
                    sdatCount++;
                    sdatSize += entry.FileContentsSize;
                }
                else if (entry.Filename.EndsWith(".trp", StringComparison.InvariantCultureIgnoreCase))
                {
                    trpCount++;
                    trpSize += entry.FileContentsSize;
                }
                else if (entry.Filename.EndsWith(".elf", StringComparison.InvariantCultureIgnoreCase))
                {
                    elfCount++;
                    elfSize += entry.FileContentsSize;
                }
                else if (entry.Filename.EndsWith(".prx", StringComparison.InvariantCultureIgnoreCase))
                {
                    prxCount++;
                    prxSize += entry.FileContentsSize;
                }
                else if (entry.Filename.EndsWith("icon0.png", StringComparison.InvariantCultureIgnoreCase))
                {
                    icon0Count++;
                    icon0Size += entry.FileContentsSize;
                }
                else if (entry.Filename.EndsWith("eboot.bin", StringComparison.InvariantCultureIgnoreCase))
                {
                    ebootCount++;
                    ebootSize += entry.FileContentsSize;
                }
                else if (entry.Filename.EndsWith("param.sfo", StringComparison.InvariantCultureIgnoreCase))
                {
                    paramSFOCount++;
                    paramSFOSize += entry.FileContentsSize;
                }
            }

            File.WriteAllText(outputTOCSummary, header.ContentID + Environment.NewLine + Environment.NewLine);

            File.AppendAllText(outputTOCSummary, "Total entries: " + header.EntryCount + Environment.NewLine + Environment.NewLine);

            File.AppendAllText(outputTOCSummary, string.Format("Total PARAM.SFOs: {0} ({1:#,##0} bytes){2}", paramSFOCount, paramSFOSize, Environment.NewLine));

            File.AppendAllText(outputTOCSummary, string.Format("Total ICON0.PNGs: {0} ({1:#,##0} bytes){2}", icon0Count, icon0Size, Environment.NewLine));

            File.AppendAllText(outputTOCSummary, string.Format("Total EBOOT.BINs: {0} ({1:#,##0} bytes){2}", ebootCount, ebootSize, Environment.NewLine));

            File.AppendAllText(outputTOCSummary, string.Format("Total SELFs: {0} ({1:#,##0} bytes){2}", selfCount, selfSize, Environment.NewLine));

            File.AppendAllText(outputTOCSummary, string.Format("Total ELFs: {0} ({1:#,##0} bytes){2}", elfCount, elfSize, Environment.NewLine));

            File.AppendAllText(outputTOCSummary, string.Format("Total SPRXs: {0} ({1:#,##0} bytes){2}", sprxCount, sprxSize, Environment.NewLine));

            File.AppendAllText(outputTOCSummary, string.Format("Total PRXs: {0} ({1:#,##0} bytes){2}", prxCount, prxSize, Environment.NewLine));

            File.AppendAllText(outputTOCSummary, string.Format("Total EDATs: {0} ({1:#,##0} bytes){2}", edatCount, edatSize, Environment.NewLine));

            File.AppendAllText(outputTOCSummary, string.Format("Total SDATs: {0} ({1:#,##0} bytes){2}", sdatCount, sdatSize, Environment.NewLine));

            File.AppendAllText(outputTOCSummary, string.Format("Total TRPs: {0} ({1:#,##0} bytes){2}{2}", trpCount, trpSize, Environment.NewLine));

            File.AppendAllText(outputTOCSummary, string.Format("Total files: {0} ({1:#,##0} bytes){2}",
                                                                             selfCount + elfCount + sprxCount + prxCount + edatCount + sdatCount + trpCount + ebootCount + paramSFOCount + icon0Count,
                                                                             selfSize + elfSize + sprxSize + prxSize + edatSize + sdatSize + trpSize + ebootSize + paramSFOSize + icon0Size,
                                                                             Environment.NewLine));

            return header;
        }

        private static long GetCheckHeader(byte[] headerCheckRaw)
        {
            using (var ms = new MemoryStream(headerCheckRaw))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x14;
                byte[] fileCountRaw = br.ReadBytes(4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(fileCountRaw);

                long fileCount = BitConverter.ToInt32(fileCountRaw, 0);

                ms.Position = 0x20;
                byte[] encryptedDataOffsetRaw = br.ReadBytes(8);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(encryptedDataOffsetRaw);

                long encryptedDataOffset = BitConverter.ToInt64(encryptedDataOffsetRaw, 0);

                ms.Position = encryptedDataOffset;

                return encryptedDataOffset + 0x10;
            }
        }

        private static byte[] GetFilePKGFile(string pkgFile, long requestedSize, PKGEntry entry, PKGHeader header)
        {
            long startOffset = header.EncryptedDataOffset + entry.FileContentsOffset;

            if (requestedSize <= 0)
                requestedSize = entry.FileContentsSize;
            else if (requestedSize > entry.FileContentsSize)
                requestedSize = entry.FileContentsSize;

            byte[] rawFile;

            using (var fs = File.Open(pkgFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var br = new BinaryReader(fs))
            {
                fs.Position = startOffset;

                rawFile = br.ReadBytes((int)requestedSize);
            }

            var extractor = new PKGExtractor();

            return extractor.DecryptRetailFile(header, entry, rawFile);
        }

        private static PKGHeader GetFilePKGHeader(string pkgFile)
        {
            using (var fs = File.Open(pkgFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var br = new BinaryReader(fs))
            {
                byte[] headerCheckRaw = br.ReadBytes(0x28);

                if (headerCheckRaw == null)
                    return null;

                long checkHeaderEnd = GetCheckHeader(headerCheckRaw);

                fs.Position = 0;

                byte[] rawCheckHeader = br.ReadBytes((int)checkHeaderEnd);

                var checkHeader = new PKGHeader(rawCheckHeader);

                if (!checkHeader.IsFinalizedValid || !checkHeader.IsMagicValid || !checkHeader.IsTypeValid)
                    return null;

                long lastOffset = checkHeader.Entries.FirstOrDefault().FileContentsOffset;

                fs.Position = 0;

                byte[] rawHeader = br.ReadBytes((int)(checkHeader.EncryptedDataOffset + lastOffset));

                checkHeader = null;

                var header = new PKGHeader(rawHeader);

                return header;
            }
        }

        private static byte[] GetLinkPKGFile(string link, long requestedSize, PKGEntry entry, PKGHeader header)
        {
            long startOffset = header.EncryptedDataOffset + entry.FileContentsOffset;

            if (requestedSize <= 0)
                requestedSize = entry.FileContentsSize;
            else if (requestedSize > entry.FileContentsSize)
                requestedSize = entry.FileContentsSize;

            if (link.EndsWith("_00.pkg"))
            {
                long pkgPart = 4687500000;

                for (int i = 0; i < 25; i++)
                    if (startOffset < pkgPart * (i + 1))
                    {
                        link = Regex.Replace(link, @"_\d\d\.pkg", string.Format("_{0:00}.pkg", i));

                        startOffset = startOffset - (pkgPart * i);

                        break;
                    }
            }

            byte[] rawFile = null;

            do
                Retry.Do(() => { rawFile = GetRawData(link, startOffset, startOffset + requestedSize); }, 1000, 5);
            while (rawFile == null || rawFile.Length == 0);

            var extractor = new PKGExtractor();

            return extractor.DecryptRetailFile(header, entry, rawFile);
        }

        private static PKGHeader GetLinkPKGHeader(string link)
        {
            byte[] headerCheckRaw = GetRawData(link, 0, 0x28);

            if (headerCheckRaw == null)
                return null;

            long checkHeaderEnd = GetCheckHeader(headerCheckRaw);

            byte[] rawCheckHeader = GetRawData(link, 0, checkHeaderEnd);

            var checkHeader = new PKGHeader(rawCheckHeader);

            if (!checkHeader.IsFinalizedValid || !checkHeader.IsMagicValid || !checkHeader.IsTypeValid)
                return null;

            long lastOffset = checkHeader.Entries.FirstOrDefault().FileContentsOffset;

            byte[] rawHeader = GetRawData(link, 0, checkHeader.EncryptedDataOffset + lastOffset);

            checkHeader = null;

            var header = new PKGHeader(rawHeader);

            return header;
        }

        private static byte[] GetRawData(string link, long start, long end)
        {
            if ((end - start) > (1024 * 1024 * 150))
                end = start + 1024 * 1024 * 150;

            var getPKGHeader = (HttpWebRequest)WebRequest.Create(link);
            getPKGHeader.UserAgent = string.Empty;
            getPKGHeader.AddRange(start, end - 1);

            try
            {
                using (var response = getPKGHeader.GetResponse())
                using (var data = response.GetResponseStream())
                using (var reader = new BinaryReader(data))
                using (var ms = new MemoryStream())
                {
                    byte[] buffer = new byte[1024 * 4];
                    int readBytes;

                    while ((readBytes = data.Read(buffer, 0, buffer.Length)) > 0)
                        ms.Write(buffer, 0, readBytes);

                    return ms.ToArray();
                }
            }
            catch (WebException)
            {
                return null;
            }
        }
    }
}
