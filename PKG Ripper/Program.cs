﻿using PKG_Extractor;
using PKG_Ripper.Functions;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace PKG_Ripper
{
    internal class Program
    {
        private const string LinkRegex = @"http://.*?\.pkg";
        private const string SplitNonFirstRegex = @"_\d[1-9]\.pkg";
        private const string TargetDirectory = "target";

        private static bool CheckParameters(string[] args)
        {
            if (args.Length != 1 && args.Length != 2)
            {
                Console.WriteLine(Assembly.GetExecutingAssembly().GetName().Name + " v" + Assembly.GetExecutingAssembly().GetName().Version + " by Dasanko, written for catalinnc." + Environment.NewLine);

                Console.WriteLine("Missing parameters." + Environment.NewLine);

                Console.WriteLine("Usage: " + Environment.NewLine);

                Console.WriteLine('"' + Assembly.GetExecutingAssembly().GetName().Name + "\" link [jobs_file]" + Environment.NewLine);

                Console.WriteLine("Example: \"" + Assembly.GetExecutingAssembly().GetName().Name +
                                  "\" http://zeus.dl.playstation.net/cdn/EP1018/NPEB01032_00/VxGedVinRnApAqiMFHrUkxuNftQhuAQKCipAOyHLrLnkXfBleTHwFmnQiBnYdrca.pkg jobs.txt");

                return false;
            }

            if (args.Length == 1 && !Regex.IsMatch(args[0], LinkRegex, RegexOptions.RightToLeft) && !File.Exists(args[0]))
            {
                Console.WriteLine("ERROR: Invalid pkg link or pkg file.");

                return false;
            }

            if (args.Length == 2 &&
                (!Regex.IsMatch(args[0], LinkRegex) || !File.Exists(args[0])) &&
                !File.Exists(args[1]) && !string.Equals(args[1], "quick", StringComparison.InvariantCultureIgnoreCase) && !string.Equals(args[1], "full", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("ERROR: Invalid pkg link, pkg file or jobs file.");

                return false;
            }

            if (args[0].Contains("/PCS") || args[0].Contains("/prod/CUSA"))
            {
                Console.WriteLine("ERROR: Unsupported pkg link.");

                return false;
            }

            if (File.Exists(args[0]))
                using (var fs = File.OpenRead(args[0]))
                using (var br = new BinaryReader(fs))
                {
                    byte[] magic = { 0x7F, 0x50, 0x4B, 0x47 };

                    byte[] thisMagic = br.ReadBytes(4);

                    if (!Enumerable.SequenceEqual(magic, thisMagic))
                    {
                        Console.WriteLine("ERROR: Unsupported pkg file.");

                        return false;
                    }

                    if (br.ReadByte() != 0x80)
                    {
                        Console.WriteLine("ERROR: Unsupported pkg file.");

                        return false;
                    }

                    fs.Position = 0x30;

                    byte[] cidRaw = br.ReadBytes(0x24);

                    string cid = Encoding.UTF8.GetString(cidRaw);

                    if (cid.Contains("-PCS"))
                    {
                        Console.WriteLine("ERROR: Unsupported pkg file.");

                        return false;
                    }
                }
            else
            {
                var request = (HttpWebRequest)WebRequest.Create(args[0]);
                request.UserAgent = string.Empty;
                request.AddRange(0x4, 0x5);

                using (var response = request.GetResponse())
                using (var data = response.GetResponseStream())
                    if (data.ReadByte() != 0x80)
                    {
                        Console.WriteLine("ERROR: Unsupported pkg link.");

                        return false;
                    }
            }

            return true;
        }

        private static void Main(string[] args)
        {
            if (!Directory.Exists(TargetDirectory))
                Directory.CreateDirectory(TargetDirectory);

            if (!CheckParameters(args))
                return;

            Console.WriteLine(string.Format("[{0}] Now working, please wait...", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss")));

            if (Regex.IsMatch(args[0], SplitNonFirstRegex))
                args[0] = Regex.Replace(args[0], SplitNonFirstRegex, @"_00.pkg");

            ProcessParameters(args);

            Console.WriteLine(string.Format("[{0}] Work complete!", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss")));
        }

        private static void ProcessParameters(string[] args)
        {
            string pkgFile = args[0];

            if (args.Length == 1)
                PKGRipFunctions.RipPKGTOC(pkgFile, TargetDirectory);
            else if (args.Length == 2 && File.Exists(args[1]))
            {
                string[] jobs = File.ReadAllLines(args[1]);

                PKGHeader header = PKGRipFunctions.RipPKGTOC(pkgFile, TargetDirectory);

                foreach (var line in jobs)
                {
                    long requestedSize = 0;

                    string targetFile = line;

                    if (line.Contains(';'))
                    {
                        string[] lineContents = line.Split(';');

                        long.TryParse(lineContents[lineContents.Length - 1], out requestedSize);

                        targetFile = lineContents[0];
                    }

                    if (targetFile.Contains("*"))
                        targetFile = targetFile.Replace("*", "");

                    PKGRipFunctions.RipPKGFiles(pkgFile, TargetDirectory, targetFile, requestedSize, header);
                }
            }
            else if (args.Length == 2 && string.Equals(args[1], "quick", StringComparison.InvariantCultureIgnoreCase))
            {
                PKGHeader header = PKGRipFunctions.RipPKGTOC(pkgFile, TargetDirectory);

                foreach (var entry in header.Entries)
                {
                    if (entry.Filename.EndsWith("eboot.bin", StringComparison.InvariantCultureIgnoreCase))
                        PKGRipFunctions.RipPKGFile(pkgFile, TargetDirectory, "eboot.bin", 0, header, entry);

                    if (entry.Filename.EndsWith("iso.bin.edat", StringComparison.InvariantCultureIgnoreCase))
                    {
                        PKGRipFunctions.RipPKGFile(pkgFile, TargetDirectory, ".edat", 0x100, header, entry);

                        return;
                    }
                }

                foreach (var entry in header.Entries)
                    if (entry.Filename.EndsWith(".edat", StringComparison.InvariantCultureIgnoreCase))
                    {
                        PKGRipFunctions.RipPKGFile(pkgFile, TargetDirectory, ".edat", 0x100, header, entry);

                        return;
                    }

                foreach (var entry in header.Entries)
                    if (entry.Filename.EndsWith(".self", StringComparison.InvariantCultureIgnoreCase))
                        if (PKGRipFunctions.RipPKGFile(pkgFile, TargetDirectory, ".self", 0x500, header, entry))
                            return;

                foreach (var entry in header.Entries)
                    if (entry.Filename.EndsWith(".sprx", StringComparison.InvariantCultureIgnoreCase))
                        if (PKGRipFunctions.RipPKGFile(pkgFile, TargetDirectory, ".sprx", 0x500, header, entry))
                            return;
            }
            else if (args.Length == 2 && string.Equals(args[1], "full", StringComparison.InvariantCultureIgnoreCase))
            {
                PKGHeader header = PKGRipFunctions.RipPKGTOC(pkgFile, TargetDirectory);

                foreach (var entry in header.Entries)
                    if (!entry.IsDirectory)
                        PKGRipFunctions.RipPKGFile(pkgFile, TargetDirectory, entry.Filename, 0, header, entry);
            }
        }
    }
}
